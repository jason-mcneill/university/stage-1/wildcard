<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>wildcards - interests</title>
		<link rel="stylesheet" type="text/css" href="styles.css">

		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css2?family=Nunito&display=swap" rel="stylesheet">
	</head>
	<body>
		<div id="board">
			<div class="interest-tiles-grid scene_element scene_element--fadeinup">
				<div class="tile-grid">
					<?php
					include('dbh.inc.php');
					$sqlget = "SELECT * FROM Tags";
					$sqldata = mysqli_query($conn, $sqlget) or die("error");

					$num = 0;

					while ($row = mysqli_fetch_array($sqldata, MYSQLI_ASSOC)) {
						if ($row['Name'] == "Feminism" || $row['Name'] == "Religion" || $row['Name'] == "LGBTQ")
						{
							echo '<input type="checkbox" name="iGroup" value="'.$row['Name'].'" checked="checked" id="i'.$num.'" />';
					    	echo '<label class="interest-tile" for="i'.$num.'">'.$row['Name'].'</label>';
						}
						else
						{
							echo '<input type="checkbox" name="iGroup" value="'.$row['Name'].'" id="i'.$num.'" />';
					    	echo '<label class="interest-tile" for="i'.$num.'">'.$row['Name'].'</label>';
						}					    

					    $num++;
					}
					?>
				</div>
			</div>
			<div class="bg scene_element scene_element--bgfadereverse"></div>		
				

			<nav class="menu">
				<div class="icon-holder">
					<a href="interests.php"><i class="selected fa fa-heart icon-menu"></i></a>
					<a href="index.php"><i class="fa fa-list icon-menu"></i></a>
					<a href=""><i class="fa fa-user icon-menu"></i></a>
				</div>
			</nav>
		</div>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.min.js"></script>
	</body>
</html>