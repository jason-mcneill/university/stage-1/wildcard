<?php 
session_start(); 
?>

<?php include 'connection.php';?>

<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>wildcards - interests</title>
		<link rel="stylesheet" type="text/css" href="styles.css">

		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css2?family=Nunito&display=swap" rel="stylesheet">
	</head>
	<body>
		<div id="board">
			<div class="card">
				<form class="post-form" action="submitPost.php" method="post" id="postForm">
					<textarea class="post-title-input" placeholder="Enter Title..." name="posttitle"></textarea>

					<textarea class="post-body-input" placeholder="Enter content..." name="postbody"></textarea>

					<label for="interests">What interest does this post belong to? </label>

					<select id="interests" style="outline: none;" name="tag">
						<?php
						// Read the tag table
						$sql = "SELECT * FROM Tags";
						$result = $conn->query($sql);

						if ($result->num_rows > 0) {
							while($row = $result->fetch_assoc()) { ?>
								<option value="<?php echo strtolower($row["Name"]); ?>"><?php echo $row["Name"]; ?></option>
								<?php
							}
						} else { ?>
							<option value="No results"></option>
							<?php
						}
						?>
					</select>

					<input type="hidden" name="username" id="username" value="<?php echo $_SESSION["username"]; ?>" />

					<button type="button-input" name="Submit" form="postForm">Submit</button>
				</form>
			</div>

			<div class="bg scene_element scene_element--bgfadereverse"></div>		
				

			<nav class="menu">
				<div class="icon-holder">
					<a href="interests.php"><i class="fa fa-heart icon-menu"></i></a>
					<a href="index.php"><i class="fa fa-list icon-menu"></i></a>
					<a href=""><i class="fa fa-user icon-menu"></i></a>
				</div>
			</nav>
		</div>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.min.js"></script>
	</body>
</html>