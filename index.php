<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>wildcards - feed</title>
		<link rel="stylesheet" type="text/css" href="styles.css">

		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css2?family=Nunito&display=swap" rel="stylesheet">
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script>
	</head>
	<body>
		<div id="board">
			<!-- <div class="bg-full scene_element scene_element--fadeoutup"></div> -->
			<div class="bg scene_element scene_element--bgfade"></div>

			<div class="control scene_element scene_element--fadein">
				<a href="create_post.php">
					<div class="circle">
						<i class="fa fa-plus"></i>
					</div>
				</a>
			</div>

			<nav class="menu">
				<div class="icon-holder">
					<a href="interests.php"><i class="fa fa-heart icon-menu"></i></a>
					<a href="index.php"><i class="selected fa fa-list icon-menu"></i></a>
					<a href=""><i class="fa fa-user icon-menu"></i></a>
				</div>
			</nav>
		</div>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.min.js"></script>
		<script>
			class Carousel {
				
				constructor(element) {
					
					this.board = element
					
					// add first two cards programmatically
					this.push()
					this.push()

					var tapped = false

					// handle gestures
					this.handle()
					
				}
				
				handle() {
					
					// list all cards
					this.cards = this.board.querySelectorAll('.card')
					
					// get top card
					this.topCard = this.cards[this.cards.length-1]
					
					// get next card
					this.nextCard = this.cards[this.cards.length-2]
					
					// if at least one card is present
					if (this.cards.length > 0) {
						
						// set default top card position and scale
						this.topCard.style.transform =
							'translateX(-50%) translateY(-46%) rotate(0deg) rotateY(0deg) scale(1)'
						
						// destroy previous Hammer instance, if present
						if (this.hammer) this.hammer.destroy()
						
						// listen for tap and pan gestures on top card
						this.hammer = new Hammer(this.topCard)
						this.hammer.add(new Hammer.Tap())
						this.hammer.add(new Hammer.Pan({
							position: Hammer.position_ALL, threshold: 0
						}))
						
						// pass events data to custom callbacks
						this.hammer.on('tap', (e) => { this.onTap(e) })
						this.hammer.on('pan', (e) => { this.onPan(e) })

					}
					
				}
				
				onTap(e) {
					
					// get finger position on top card
					let propX = (e.center.x - e.target.getBoundingClientRect().left) / e.target.clientWidth
					
					// get degree of Y rotation (+/-15 degrees)
					let rotateY = 10 * (propX < 0.05 ? -1 : 1)
					
					// change the transition property
					this.topCard.style.transition = 'transform 100ms ease-out'
					this.topCard.style.transition = 'width 0.2s, height 0.3s'
					
					if (!this.tapped) {
						// rotate
						this.topCard.style.transform =
							'translateX(-50%) translateY(-50%) rotate(0deg) rotateY(' + rotateY + 'deg) scale(1)'

						this.topCard.style.width = "1020px"
						
						// wait transition end
						setTimeout(() => {
							// reset transform properties
							this.topCard.style.transform =
								'translateX(-50%) translateY(-50%) rotate(0deg) rotateY(0deg) scale(1)'
						}, 100)

						this.tapped = true
					} else {
						// rotate
						this.topCard.style.transform =
							'translateX(-50%) translateY(-46%) rotate(0deg) rotateY(' + rotateY + 'deg) scale(1)'

						this.topCard.style.width = "720px"
						
						// wait transition end
						setTimeout(() => {
							// reset transform properties
							this.topCard.style.transform =
								'translateX(-50%) translateY(-46%) rotate(0deg) rotateY(0deg) scale(1)'
						}, 100)

						this.tapped = false;
					}
					
				}
				
				onPan(e) {
					
					this.tapped = false
					this.topCard.style.width = "720px"

					if (!this.isPanning) {
						
						this.isPanning = true
						
						// remove transition properties
						this.topCard.style.transition = null
						if (this.nextCard) this.nextCard.style.transition = null
						
						// get top card coordinates in pixels
						let style = window.getComputedStyle(this.topCard)
						let mx = style.transform.match(/^matrix\((.+)\)$/)
						this.startPosX = mx ? parseFloat(mx[1].split(', ')[4]) : 0
						this.startPosY = mx ? parseFloat(mx[1].split(', ')[5]) : 0
						
						// get top card bounds
						let bounds = this.topCard.getBoundingClientRect()
						
						// get finger position on top card, top (1) or bottom (-1)
						this.isDraggingFrom =
							(e.center.y - bounds.top) > this.topCard.clientHeight / 2 ? -1 : 1
						
					}
					
					// calculate new coordinates
					let posX = e.deltaX + this.startPosX
					let posY = e.deltaY + this.startPosY
					
					// get ratio between swiped pixels and the axes
					let propX = e.deltaX / this.board.clientWidth
					let propY = e.deltaY / this.board.clientHeight
					
					// get swipe direction, left (-1) or right (1)
					let dirX = e.deltaX < 0 ? -1 : 1
					
					// calculate rotation, between 0 and +/- 45 deg
					let deg = this.isDraggingFrom * dirX * Math.abs(propX) * 45
					
					// calculate scale ratio, between 95 and 100 %
					let scale = (95 + (5 * Math.abs(propX))) / 100
					
					// move top card
					this.topCard.style.transform =
						'translateX(' + posX + 'px) translateY(' + posY + 'px) rotate(' + deg + 'deg) rotateY(0deg) scale(1)'
					
					// scale next card
					if (this.nextCard) this.nextCard.style.transform =
						'translateX(-50%) translateY(-50%) rotate(0deg) rotateY(0deg) scale(' + scale + ')'
					
					if (e.isFinal) {
						
						this.isPanning = false
						
						let successful = false
						
						// set back transition properties
						this.topCard.style.transition = 'transform 200ms ease-out'
						if (this.nextCard) this.nextCard.style.transition = 'transform 100ms linear'
						
						// check threshold
						if (propX > 0.25 && e.direction == Hammer.DIRECTION_RIGHT) {
				  
							successful = true
							// get right border position
							posX = this.board.clientWidth
				   
						} else if (propX < -0.25 && e.direction == Hammer.DIRECTION_LEFT) {
				  
							successful = true
							// get left border position
							posX = - (this.board.clientWidth + this.topCard.clientWidth)
				   
						} else if (propY < -0.25 && e.direction == Hammer.DIRECTION_UP) {
				  
							successful = true
							// get top border position
							posY = - (this.board.clientHeight + this.topCard.clientHeight)
				  
						}
						
						if (successful) {
				
							// throw card in the chosen direction
							this.topCard.style.transform =
								'translateX(' + posX + 'px) translateY(' + posY + 'px) rotate(' + deg + 'deg)'
						
							// wait transition end
							setTimeout(() => {
								// remove swiped card
								this.board.removeChild(this.topCard)
								// add new card
								this.push()
								// handle gestures on new top card
								this.handle()
							}, 200)
						
						} else {
				  
							// reset cards position
							this.topCard.style.transform =
								'translateX(-50%) translateY(-46%) rotate(0deg) rotateY(0deg) scale(1)'
							if (this.nextCard) this.nextCard.style.transform =
								'translateX(-50%) translateY(-50%) rotate(0deg) rotateY(0deg) scale(0.95)'
				  
						}
				
					}
					
				}
				
				push() {
					
					// Create card
					let card = document.createElement('div')
					// Add post to card
					let post = document.createElement('div')
					// Add header to post
					let header = document.createElement('header')
					// Add title to post
					let title = document.createElement('span')
					// Add interest to post
					let interest = document.createElement('interest')
					// Add body to post
					let body = document.createElement('div')
					// Add text to body
					let text = document.createElement('pre')

					card.classList.add('card')
					card.classList.add('scene_element')
					card.classList.add('scene_element--cardfade')
					post.classList.add('post')
					header.classList.add('header')
					title.classList.add('title')
					interest.classList.add('interest')
					body.classList.add('body')
					text.classList.add('text')

					card.appendChild(post)
					post.appendChild(header)
					header.appendChild(title)
					header.appendChild(interest)
					post.appendChild(body)
					body.appendChild(text)





					var temp = function(callback) {
						$.ajax({
							url: "getRandomPost.php",
							type: "GET",
							success: function (result) {
								callback(result);
							},
							dataType:"json"
						});
					}


					temp(function(result) {
						title.textContent += result.Title;
						text.textContent += result.Body;

						var tempInterest = function(callback) {
							$.ajax({
								url: "getPostTag.php",
								type: "POST",
								data: ({cardID: result.CardsID}),
								success: function (data) {
									callback(data);
									console.log(data);
								},
								dataType:"json"
							});
						}

						tempInterest(function(data) {
							console.log(data);
							interest.textContent += data;
						});
					});

					

					// card.style.backgroundImage = "url('https://picsum.photos/320/320/?random=" + Math.round(Math.random()*1000000) + "')"
					
					if (this.board.firstChild) {
						this.board.insertBefore(card, this.board.firstChild)
					} else {
						this.board.append(card)
					}
					
				}
				
			}

			let board = document.querySelector('#board')

			let carousel = new Carousel(board)
		</script>
	</body>
</html>