<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>wildcards - login</title>
		<link rel="stylesheet" type="text/css" href="styles.css">

		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css2?family=Nunito&display=swap" rel="stylesheet">
	</head>
	<body>
		<div id="board">
			<div class="card display-table">
				<div class="login-form">
					<div class="login-title">Login</div>

					<form class="start-form" action="postLogin.php" method="post">
						<input type="text" placeholder="Enter Username" name="username" required>	

						<input type="password" placeholder="Enter Password" name="password" required>

						<button type="submit">Login</button>
					</form>
				</div>
			</div>

			<div class="bg-full"></div>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.min.js"></script>
	</body>
</html>